// components/ProtectedRoute.tsx

import { useRouter } from "next/router";
import { useSelector } from "react-redux";
import { RootState } from "../../store/store";
import { ReactNode } from "react";

interface ProtectedRouteProps {
  allowedRoles: string[];
  children: ReactNode; // Define children as ReactNode
}
const AuthLayout: React.FC<ProtectedRouteProps> = ({
  allowedRoles,
  children,
}) => {
  const router = useRouter();
  const userRole = useSelector((state: RootState) => state.auth.user?.role);

  if (!userRole || !allowedRoles.includes(userRole)) {
    router.push("/login"); // Redirect to login page if user is not authenticated or doesn't have the required role
    return null;
  }

  return <>{children}</>;
};

export default AuthLayout;
