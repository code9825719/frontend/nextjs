import React from "react";
import OTPInput from "react-otp-input";

interface OtpProps {
  setOtp: () => void;
  otp: string;
}
export const OTPInputs: React.FC<OtpProps> = ({ setOtp, otp }) => {
  return (
    <OTPInput
      value={otp}
      onChange={setOtp}
      numInputs={6}
      shouldAutoFocus
      renderSeparator={<span></span>}
      inputStyle="inputStyle"
      renderInput={(props: any) => (
        <input
          {...props}
          //   className="" classes
        />
      )}
    />
  );
};
