// store/userSlice.ts

import { createSlice, PayloadAction } from "@reduxjs/toolkit";

interface UserState {
  user: any[]; // Define your user state structure here
}

const initialState: UserState = {
  user: [],
};

const userSlice = createSlice({
  name: "user",
  initialState,
  reducers: {
    setLoginUser(state, action: PayloadAction<any>) {
      state.user = action.payload;
    },
    // Add other user-related reducers as needed
  },
});

export const { setLoginUser } = userSlice.actions;
export default userSlice.reducer;
