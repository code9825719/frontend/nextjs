// store/authSlice.ts

import { createSlice, PayloadAction } from "@reduxjs/toolkit";

interface AuthState {
  user: { id: string, name: string, role: string } | null;
  // Other authentication-related state
}

const initialState: AuthState = {
  user: null,
  // Other initial state
};

export const authSlice = createSlice({
  name: "auth",
  initialState,
  reducers: {
    login(
      state,
      action: PayloadAction<{ id: string, name: string, role: string }>,
    ) {
      state.user = action.payload;
    },
    logout(state) {
      state.user = null;
    },
  },
});

export const { login, logout } = authSlice.actions;
