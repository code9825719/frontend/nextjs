export const authService = {
  login: "/auth/login",
  forgotpassword: "/auth/forget-password",
  verifyToken: "/auth/verify-token",
  resetPass: "/auth/set-new-password",
  logout: "/auth/logout",
};
