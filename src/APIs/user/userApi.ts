export const addUser = async (payload: any) => {
  try {
    const response = await axiosInterceptorInstance.post(
      `${process.env.NEXT_PUBLIC_BACKEND_URL}${userService.add}`,
      payload,
    );
    if (!response.data) {
      throw new Error(response.data?.message || "request failed");
    }
    return response;
  } catch (error) {
    console.error("Error:", error);
    throw new Error("Add user request failed");
  }
};

import axiosInterceptorInstance from "@/utils/axiosInterceptor";
import { userService } from "./userService";

export const getUser = async (id: string | string[]) => {
  const response = await axiosInterceptorInstance.get(
    `${process.env.NEXT_PUBLIC_BACKEND_URL}${userService.get}/${id}`,
  );
  if (!response?.data) {
    throw new Error(response?.data.message || "request failed");
  }
  return response;
};

export const updateUser = async (payload: any) => {
  try {
    const response = await axiosInterceptorInstance.put(
      `${process.env.NEXT_PUBLIC_BACKEND_URL}${userService.update}/${payload.id}`,
      payload,
    );
    if (!response.data) {
      throw new Error(response.data?.message || "request failed");
    }
    return response;
  } catch (error) {
    console.error("Error:", error);
    throw new Error("Add user request failed");
  }
};

export const deleteUser = async (payload: any) => {
  try {
    const response = await axiosInterceptorInstance.delete(
      `${process.env.NEXT_PUBLIC_BACKEND_URL}${userService.delete}/${payload.email}`,
    );
    if (!response.data) {
      throw new Error(response.data?.message || "request failed");
    }
    return response;
  } catch (error) {
    throw new Error("Add user request failed");
  }
};
