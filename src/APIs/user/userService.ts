export const userService = {
  update: "/user/update",
  delete: "/users",
  get: "/user/get",
  add: "/user/add",
};
