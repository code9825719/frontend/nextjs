import { notification } from "antd";

// Define a type for the allowed notification types
type NotificationType = "success" | "info" | "warning" | "error";

export const openNotification = (
  type: NotificationType,
  message: string,
  description: string | React.ReactNode,
  onClose?: () => void,
): void => {
  notification[type]({
    message: message,
    description: description,
    onClose: onClose,
    duration: 1,
  });
};
