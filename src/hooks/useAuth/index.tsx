import {
  forgotPassword,
  resetPassword,
  userLogin,
  verifyOTP,
} from "@/APIs/Auth/authAPI";
import { setLoginUser } from "@/store/slices/user/userSlice";
import { openNotification } from "@/utils/notificationPopup";
import { useCallback, useState } from "react";
import { useDispatch } from "react-redux";

export const useAuth = () => {
  const dispatch = useDispatch();
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState<string | null>(null);

  const [errorCode, setErrorCode] = useState<number>(0);
  const [success, setSuccess] = useState<boolean>(false);

  const handleLogin = useCallback(
    async (payload: any) => {
      try {
        setError(null);
        setLoading(true);
        const response = await userLogin(payload);
        if (response.status === 200 || response.status === 201) {
          dispatch(setLoginUser(response.data));
          localStorage.setItem("authToken", JSON.stringify(response.data));
          setSuccess(true);
          setLoading(false);
        }
      } catch (error: any) {
        setSuccess(false);
        const errorMessage =
          error.response?.data?.message || "An error occurred";
        setError(errorMessage);
        setErrorCode(error.response?.status);
        setLoading(false);
      }
    },
    [dispatch],
  );

  const handleForgotPassword = useCallback(async (payload: string) => {
    try {
      setError(null);
      setLoading(true);
      const response = await forgotPassword(payload);

      if (response.status === 200 || response.status === 201) {
        //   dispatch(setuser(response.data));
        //   setSuccess(true);
        //   setLoading(false);
      }
      return true;
    } catch (error: any) {
      setSuccess(false);
      const errorMessage = error.response?.data?.message || "An error occurred";
      setError(errorMessage);
      setErrorCode(error.response?.status);
      setLoading(false);
      return false;
    }
  }, []);

  const handleLogout = useCallback(async () => {
    try {
      setError(null);
      setLoading(true);
      // const response = await userLogout();
      // if (response.status === 200 || response.status === 201) {
      // handleLogoutUser();
      // }
      return true;
    } catch (error: any) {
      setSuccess(false);
      const errorMessage = error.response?.data?.message || "An error occurred";
      // handleLogoutUser();
      setError(errorMessage);
      setErrorCode(error.response?.status);
      setLoading(false);
    }
  }, []);

  const handleResetPassword = useCallback(async (payload: any) => {
    try {
      setError(null);
      setLoading(true);
      const response = await resetPassword(payload);

      if (response.status === 200 || response.status === 201) {
        //   dispatch(setuser(response.data));
        //   setSuccess(true);
        //   setLoading(false);
      }
      return true;
    } catch (error: any) {
      setSuccess(false);
      openNotification("error", "Error", error?.response?.data?.error);
      const errorMessage = error.response?.data?.message || "An error occurred";
      setError(errorMessage);
      setErrorCode(error.response?.status);
      setLoading(false);
      return false;
    }
  }, []);

  const handleVerifyOTP = useCallback(async (payload: any) => {
    try {
      setError(null);
      setLoading(true);
      const response = await verifyOTP(payload);
      if (response?.data?.data?.email) {
        typeof window !== "undefined" &&
          localStorage.setItem("respEmail", response?.data?.data?.email);
      }
      return { status: true, user: response };
    } catch (error: any) {
      openNotification("error", "Error", error?.response?.data?.error);
      setSuccess(false);
      const errorMessage = error.response?.data?.message || "An error occurred";
      setError(errorMessage);
      setErrorCode(error.response?.status);
      setLoading(false);

      return { status: false };
    }
  }, []);
  return {
    handleResetPassword,
    handleLogin,
    handleVerifyOTP,
    handleForgotPassword,
    handleLogout,
    loading,
    error,
    success,
    setError,
    errorCode,
    setErrorCode,
    setLoading,
  };
};
