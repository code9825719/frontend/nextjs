import { getUser, updateUser } from "@/APIs/user/userApi";
import { setLoginUser } from "@/store/slices/user/userSlice";
import { openNotification } from "@/utils/notificationPopup";
import { useCallback, useState } from "react";
import { useDispatch } from "react-redux";

export const useLoginUser = () => {
  const dispatch = useDispatch();
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState<string | null>(null);
  const [errorCode, setErrorCode] = useState<number>(0);
  const [success, setSuccess] = useState<boolean>(false);
  const handleUpdate = useCallback(
    async (payload: any) => {
      try {
        setError(null);
        setLoading(true);
        const response = await updateUser(payload);
        dispatch(setLoginUser(response?.data?.data?.updateduser));
        openNotification("success", "Success", "Profile updated successfully!");

        return { status: true };
      } catch (error: any) {
        setSuccess(false);
        const errorMessage =
          error.response?.data?.error ||
          error.response?.data?.message ||
          "An error occurred";
        setError(errorMessage);
        setErrorCode(error.response?.status);
        setLoading(false);
        return { status: false };
      }
    },
    [dispatch],
  );
  const handleGetUser = useCallback(async (payload: string | string[]) => {
    try {
      setError(null);
      setLoading(true);
      const response = await getUser(payload);

      return { status: true, user: response?.data?.data?.user };
    } catch (error: any) {
      setSuccess(false);
      const errorMessage =
        error.response?.data?.error ||
        error.response?.data?.message ||
        "An error occurred";
      setError(errorMessage);
      setErrorCode(error.response?.status);
      setLoading(false);
      return { status: false };
    }
  }, []);
  return {
    handleUpdate,
    handleGetUser,
    loading,
    setLoading,
    error,
    success,
    setError,
    errorCode,
    setErrorCode,
  };
};
