export const deleteData = (data: any, obj: any) => {
  const _data = data.filter((item: any) => item.id !== obj.id);
  return _data;
};
