import { Modal } from "antd";
import React from "react";
// import { useSelector } from "react-redux";

interface ModalProp {
  open?: boolean;
  children?: React.ReactNode;
  onCancel?: () => void;
  className?: string;
  width?: number;
  footer?: boolean;
  centered?: boolean;
}
const CustomeModal = (props: ModalProp) => {
  const { open, children, onCancel, className, footer, width, centered } =
    props;

  return (
    <Modal
      open={open}
      onCancel={onCancel}
      centered={centered}
      footer={footer}
      width={width}
      className={className}
    >
      <div className="P-8 mb-10">{children}</div>
    </Modal>
  );
};

export default CustomeModal;
