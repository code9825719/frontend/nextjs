import React from "react";
import CustomeModal from "./customModal";

interface Props {
  title: string;
  message: string;
  onConfirm: () => void;
  handleCancel: () => void;
  confirmButtonText: string;
  cancelButtonText: string;
  visible: boolean;
}
const ConfirmationModal = (props: Props) => {
  const {
    title,
    message,
    onConfirm,
    confirmButtonText,
    cancelButtonText,
    handleCancel,
    visible,
  } = props;
  return (
    <CustomeModal open={visible} footer={false} onCancel={handleCancel}>
      <div className="text-center">
        <div>{title}</div>
        <div className="mt-3">
          <p>{message}</p>
        </div>
        <div className="mt-6">
          <button
            onClick={handleCancel}
            className="me-3 text-white hover:text-black bg-danger hover:bg-white font-medium rounded-lg px-5 py-2 text-center"
          >
            {cancelButtonText}
          </button>
          <button
            onClick={onConfirm}
            className="text-white hover:text-black bg-[#FFFFFF33] hover:bg-white font-medium rounded-lg px-4 py-2 text-center mb-2 mt-3 lg:mt-0"
          >
            {confirmButtonText}
          </button>
        </div>
      </div>
    </CustomeModal>
  );
};

export default ConfirmationModal;
