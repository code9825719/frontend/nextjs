module.exports = {
  extends: ["next/core-web-vitals", "prettier"],
  plugins: ["prettier"],
  rules: {
    "prettier/prettier": [
      "error",
      {
        singleQuote: false,
        parser: "flow",
      },
    ],
    "linebreak-style": ["error", "unix"],
    "no-unused-vars": "error",
  },
};
